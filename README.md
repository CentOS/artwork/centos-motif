# CentOS Artistic Motif

CentOS artistic motif provides the "one unique look and feel" necessary to
connect different visual manifestations of the CentOS Project (e.g.,
distributions, websites, and promotion media.) If you are interested in
contributing artwork to CentOS Project and express your full [graphic
design](https://en.wikipedia.org/wiki/Graphic_design) creative potential, the
CentOS artistic motif is the place for you.

CentOS artistic motif is limited by technical restrictions described in
[GNOME backgrounds reference](https://developer.gnome.org/hig/reference/backgrounds.html).

## Concept

Neuron Synapse:

> _In the nervous system, a synapse is a structure that permits a neuron (or
> nerve cell) to pass an electrical or chemical signal to another neuron or to
> the target effector cell._ --- [wikipedia.org/wiki/Synapse](https://en.wikipedia.org/wiki/Synapse).

| Day                                             | Night                                             |
| ----------------------------------------------- | ------------------------------------------------- |
| ![Neuron Synapse - Day](Final/preview-day.webp) | ![Neuron Synapse - Day](Final/preview-night.webp) |

## Usage

CentOS artistic motif is a reusable image. It implements the connection between
different visual manifestations. It does not present project-specific branding
or textual information because it the baseline of other images that do provide
project-specific branding, or textual information. Likewise, the artistic motif
must be suitable for using as desktop wallpaper, where the challenge is
delivering a balanced image between
[aesthetic](https://en.wikipedia.org/wiki/Aesthetics) and no visual
distractions, that may take the user eyes away from the main work.

To use the artistic motif final images in your derivative work, do the
following:

1. Download the artistic motif final files into `centos-motif` directory:

   ```
   podman run \
     --rm \
     -v $(PWD)/centos-motif:/srv/centos-motif-local \
     registry.gitlab.com/centos/artwork/centos-motif:latest \
     sh -c "install -t /srv/centos-motif-local/ /srv/centos-motif/*"
   ```

1. In Inkscape, import the artistic motif final image using an image references
   to produce new images based on it.

## License

Copyright (C) 2024 Alain Reguera Delgado

This is a free work, you can copy, distribute, and modify it under the terms of
the [Free Art License](https://artlibre.org/licence/lal/en/).
